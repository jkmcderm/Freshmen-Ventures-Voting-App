# Installation Instructions

### Python and mod_wsgi

* Make sure Python version >= 3.3, 3.5 preferred.

* Install mod-wsgi:  
**Debian**  
    `sudo apt-get install libapache2-mod-wsgi-py3`  
**CentOS**  
`wget https://github.com/GrahamDumpleton/mod_wsgi/archive/4.5.7.zip
unzip 4.5.7.zip
cd mod_wsgi-4.5.7
./configure --with-python=/usr/bin/python3
make
sudo make install`

* Install Flask with python 3's pip utility:
    `python3 -m pip install Flask`

### Install Flask extensions

* First, make sure wheel is installed: `pip3 install wheel`

* Install SQLAlchemy: `pip3 install sqlalchemy`

* Install flask-sqlalchemy: `pip3 install flask-sqlalchemy`

* Install flask-mysqldb: `pip3 install flask-mysqldb`

* Install pillow for thumbnail support `pip3 install pillow`

* Install image `pip3 install image`

* For authentication (optional) install flask_sso `pip3 install flask-sso`

* For Windows, also install pymysql: `python -m pip install pymysql` and set in config `db_engine` to `mysql+pymysql`

#### Notes

* If `pip3` does not work, you can execute `python3 -m pip <package_to_install>`

* If there is an error installing flask-mysqldb, you likely need to install the `python3-dev` package.

* Other errors while installing with `pip` may be solved by removing your distro's version and downloading them from PyPA:

```
sudo apt-get remove python3-pip
wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
```

### Configure Apache

* In httpd.conf, set a URL for the script, and allow it to be executed by adding something similar to the following:
```
    WSGIScriptAlias /ventures /usr/local/ampps/www/wsgi-scripts/ventures.wsgi

    <Directory /usr/local/ampps/www/wsgi-scripts>
        Require all granted
    </Directory>
```

### Application Setup

* The .wsgi script (and the rest of the package) should be in <Document Root>/wsgi-scripts

* The folder structure should be similar to the following:
```
<Document Root>/
    wsgi_scripts/
        ventures.wsgi
        ventures/
            app.py
            ..etc..
```


### Authentication Setup

* Support is optional. As per above, to use SSO authentication, you'll need to install `flask-sso`

* You'll need to install and setup shibboleth

* For UM, most of the shibboleth-specific defaults will work.

* The app will need a shibboleth-secured subdirectory called `login`

* In `/etc/httpd/conf.d/shib.conf`:

```
<Location /ventures/login>
  AuthType shibboleth
  ShibRequestSetting requireSession 1
  require shib-session
</Location>
```

* Make sure in `config.py` that `auth_type` is set to `sso`.

### Last Steps

* Set desired configuration in application's config files (TBA)

* Lastly, run `python3 ventures.wsgi -c` or access the `setup` link from within the app to build the database.

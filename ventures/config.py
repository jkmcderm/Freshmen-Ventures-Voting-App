# Voting App Config File

_config = {
    # A directory that sits next to app.py.
    'upload_dir': "/uploads",
    'upload_ext': ['png', 'jpg', 'jpeg', 'gif'],
    'secret_key': "<set_this_to_something_random>",  # Used for session signing

    # Voting Config
    'votes_per_user': 3,
    'all_at_once': False,  # False to allow 'staggered' voting.
    'thumbnail_size': 128,
    'whitelist': True,
    'admin_vote': False,  # Allow admins to vote?

    # Authentication Config
    # For SSO, you will need to set a shibboleth auth dir (in shib.conf)
    # under the ventures directory, example: 'ventures/login'
    # This folder need not actually exist.
    'auth_type': 'sso',  # Currently only '' or 'sso' via shibboleth
    'admins': ['spolston', 'jkmcderm'],
    'SSO_ATTRIBUTE_MAP': {
        'uid': (True, 'uid'),
        'eppn': (False, 'eppn'),
        'affiliation': (False, 'affiliation'),
        'unscoped-affiliation': (False, 'unscoped-affiliation'),
        'entitlement': (False, 'entitlement'),
        'targeted-id': (False, 'targeted-id'),
        'persistent-id': (False, 'persistent-id'),
    },

    # DB Config
    'db_engine': "mysql",  # Use 'mysql+pymysql' when on windows.
    'db_name': "test_db",
    'db_user': "test_user",
    'db_pass': "test_user_pass",
    'db_host': "localhost",
    'db_socket': "",  # Only on unix
    'db_port': "",  # blank for Default

    'DEBUG': True
}


# Do not edit past here ---------------
def parse_config(conf):
    # Build DB URI
    uri = (conf['db_engine'] + "://" +
           conf['db_user'] + ":" +
           conf['db_pass'] + "@" +
           conf['db_host'])

    if conf['db_port']:
        uri += ":" + conf['db_port']

    uri += "/" + conf['db_name']

    if conf['db_socket'] and (conf['db_engine'] == "mysql"):
        uri += "?unix_socket=" + conf['db_socket']

    conf['db_uri'] = uri

    print(">>><<<" + uri)

    return conf

config = parse_config(_config)

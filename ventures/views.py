from flask import render_template, request, redirect, send_from_directory, \
                  flash, url_for, session

from ventures import app, db
import ventures.models as models
import ventures.utils as utils


@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('home.html')

    elif request.method == 'POST':
        if not app.config['AUTH_TYPE'] == 'sso':
            if 'login' in request.form:
                session['user'] = {}
                session['user']['username'] = request.form['username']
                if 'admin' in request.form['username'].lower().strip():
                    session['user']['is_admin'] = True
                    return redirect(url_for('entries_page'))
                else:
                    return redirect(url_for('vote_page'))

        if 'logout' in request.form:
            session.pop('user', None)

        return redirect(request.url)


@app.route('/setup', methods=['GET', 'POST'])
def setup():
    # Only let logged in admins access this page.
    if 'user' in session:
        if 'is_admin' not in session['user']:
            return redirect(url_for('index'))
    else:
        return redirect(url_for('index'))

    if request.method == 'GET':
        return render_template('setup.html')

    elif request.method == 'POST':

        try:
            utils.init_db()
            flash('Database initialized successfully')
        except:
            flash('There was a problem initializing database.')

        file_error = utils.clear_files()

        if file_error:
            flash("Error when trying to clear the uploads folder.")

        elif 'debugdata' in request.form:
            try:
                lst = utils.debug_populate()
                flash('Debug data successfully added: </br>' + str(lst))
            except:
                flash('Error when adding debug data.')

        return redirect(request.url)


@app.route('/entries', methods=['GET', 'POST'])
def entries_page():
    # Only let logged in admins access this page.
    if 'user' in session:
        if 'is_admin' not in session['user']:
            return redirect(url_for('index'))
    else:
        return redirect(url_for('index'))

    # POST Request ------------------------------
    if request.method == 'POST':
        if 'addentry' in request.form:
            # First, make sure all necessary parts are included.

            if request.form['name'] and \
               request.form['desc'] and \
               request.files['file']:

                # Handle file upload and thumbnail
                file = request.files['file']
                if file.filename == '':
                    flash('No selected file')
                if file and utils.allowed_file(file.filename):
                    utils.add_entry(request.form['name'],
                                    request.form['desc'], file)
                    flash('Entry successfully added!')

            else:
                flash('Fill out all fields to add an entry')

        elif 'delentry' in request.form:
            to_del = request.form.getlist('check_entry[]')
            if to_del:
                utils.rm_entry(to_del)
                flash('Entry deleted successfully.')
            else:
                flash('No entries selected for deletion.')

        elif 'editentry' in request.form:
            # Update text data
            for key in request.form.keys():
                if "_" in key:
                    entry_id = key.split('_')[-1]
                    if entry_id.isdigit():
                        entry = models.Entry.query.get(entry_id)
                        if "edit_name_" in key:
                            entry.name = request.form["edit_name_"+entry_id]
                        if "edit_desc_" in key:
                            entry.desc = request.form["edit_desc_"+entry_id]
                        if "edit_order_" in key:
                            entry.order = request.form["edit_order_"+entry_id]

                        db.session.commit()

            for key in request.files.keys():
                # Update file data
                if key[-1].isdigit():
                    entry_id = key[-1]
                    entry = models.Entry.query.get(key[-1])
                    if "edit_image_" in key:

                        file = request.files[key]
                        if file.filename == '':
                            flash('No selected file')
                        elif file and utils.allowed_file(file.filename):
                            utils.rm_image(entry)
                            filename = utils.save_image(file)
                            entry.image = filename
                            flash('Image updated for entry: ' + entry.name)

                            db.session.commit()

            flash('Edit successful!')

        elif 'clear_votes' in request.form:
            utils.clear_votes()

        return redirect(request.url)

    # GET Request -------------------------------
    elif request.method == 'GET':

        edict, error = utils.get_entries()
        if error:
            return error
        else:
            return render_template('entries.html', edict=edict)

        # TODO: Interface to add one or more Entry objects. (Like a '+' button)


@app.route('/users', methods=['GET', 'POST'])
def users_page():
    # Only let logged in admins access this page.
    if 'user' in session:
        if 'is_admin' not in session['user']:
            return redirect(url_for('index'))
    else:
        return redirect(url_for('index'))

    # POST Request ------------------------------
    if request.method == 'POST':
        if 'add_voters' in request.form:
            voter_list = request.form['usernames']
            result = utils.add_voters(voter_list)

            if not result:
                flash("Problem adding to whitelist. Was the data entered\
                      correctly?")

        elif 'remove_voters' in request.form:
            utils.rm_voters(request.form['usernames'],
                            request.form.get('delete_votes'))

        elif 'clear_voters' in request.form:
            utils.rm_all_voters(request.form.get('delete_votes'))

        return redirect(request.url)

    # GET Request -------------------------------
    elif request.method == 'GET':

        voters, error = utils.get_voters()
        if error:
            return error
        else:
            return render_template('users.html', voters=voters)


@app.route('/vote', methods=['GET', 'POST'])
def vote_page():
    # Only let logged in users access this page.
    if 'user' not in session:
        return redirect(url_for('index'))

    # POST Request ------------------------------
    if request.method == 'POST' and 'vote' in request.form:

        # First, make sure user is eligible to vote.
        if app.config['USE_WHITELIST']:
            if not utils.is_eligible(session['user']['username']):
                flash("Sorry, you are not eligible to vote. If you think \
                      this is an error, try contacting an admin.")
                return redirect(request.url)

        # Disallow votes from admins if setting is turned off.
        if 'is_admin' in session['user'] and not app.config['ADMIN_VOTE']:
            flash("Administrators are not allowed to vote.")
            return redirect(url_for('index'))

        # Get or create uservote object.
        new_user = False
        uv = models.Uservote.query.filter_by(
                                username=session['user']['username']).first()
        if not uv:
            new_user = True
            uv = models.Uservote(session['user']['username'])

        # Get votes (list of entry IDs)
        votes = request.form.getlist('vote_entry[]')

        # Make sure uservote.votes + current_votes don't exceed max votes.
        valid = True
        if app.config['VOTE_ALL_AT_ONCE']:
            if len(votes) != app.config['MAX_VOTES']:
                flash('Did not vote for exactly ' +
                      str(app.config['MAX_VOTES']) + ' entries.')
                valid = False
        if len(votes) + len(uv.uservotes.all()) > app.config['MAX_VOTES']:
            flash('Too many votes, can only vote on ' +
                  str(app.config['MAX_VOTES']) + ' entries.')
            valid = False

        if valid:
            if new_user:
                db.session.add(uv)

            # Get rid of any duplicate votes just in case.
            try:
                existing_votes, v_error = utils.get_votes(
                                                session['user']['username'])
                for i in range(len(existing_votes)):
                    existing_votes[i] = str(existing_votes[i])
                conflicts = list(set(votes).intersection(existing_votes))

                for conflict in conflicts:
                    # Remove all occurences of the confclict from votelist.
                    votes = [x for x in votes if x != conflict]
                    flash('Duplicate vote for entry: ' +
                          models.Entry.query.get(conflict).name)

            except ValueError:
                pass  # No problem, just no duplicates found.

            # Get objects out of vote IDs.
            vlist = []
            for vote in votes:
                vlist.append(models.Vote(uv, models.Entry.query.get(vote)))

            for vote in vlist:
                db.session.add(vote)

            if vlist:
                flash('Vote successful!')

            db.session.commit()

        return redirect(request.url)

    # GET Request -------------------------------
    # TODO: Disable or otherwise make apparent already voted-for entries.

    elif request.method == 'GET':
        edict, e_error = utils.get_entries()
        votes, v_error = utils.get_votes(session['user']['username'])

        error = []
        if isinstance(e_error, list):
            error += e_error
        if isinstance(v_error, list):
            error += v_error

        if error:
            return error
        else:
            return render_template('vote.html', edict=edict, votes=votes)


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_DIR'], filename)
